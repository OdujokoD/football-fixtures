package com.odujokod.footballfixtures.viewmodel;

import android.app.Application;
import android.util.Log;

import com.odujokod.footballfixtures.model.Competition;
import com.odujokod.footballfixtures.repository.CompetitionRepository;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

public class CompetitionViewModel extends AndroidViewModel {

    private CompetitionRepository repository;
    private LiveData<List<Competition>> competitions;

    public CompetitionViewModel(@NonNull Application application) {
        super(application);
        repository = new CompetitionRepository(application);
        competitions = repository.getCompetitions();
    }

    public void insert(Competition competition){
        repository.insert(competition);
    }

    public void update(Competition competition){
        repository.update(competition);
    }

    public void delete(Competition competition){
        repository.delete(competition);
    }

    public void deleteAllCompetitions(){
        repository.deleteAllCompetitions();
    }

    public LiveData<List<Competition>> getAllCompetitions() {
        Log.d("View model", "CompetitionViewModel: " + competitions);
        return competitions;
    }
}
