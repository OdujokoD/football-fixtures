package com.odujokod.footballfixtures.repository;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import com.odujokod.footballfixtures.db.CompetitionDao;
import com.odujokod.footballfixtures.db.FootballFixtureDB;
import com.odujokod.footballfixtures.model.Competition;
import com.odujokod.footballfixtures.model.CompetitionResponse;
import com.odujokod.footballfixtures.network.ConnectionEndpoint;
import com.odujokod.footballfixtures.network.ConnectionSetup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CompetitionRepository {

    private CompetitionDao competitionDao;
    private LiveData<List<Competition>> competitions;

    public CompetitionRepository(Application application){
        FootballFixtureDB database = FootballFixtureDB.getInstance(application);
        competitionDao = database.competitionDao();
        competitions = competitionDao.getAllCompetitions();
    }

    public void insert(Competition competition){
        new InsertCompetitionAsyncTask(competitionDao).execute(competition);
    }

    public void update(Competition competition){
        new UpdateCompetitionAsyncTask(competitionDao).execute(competition);
    }

    public void delete(Competition competition){
        new DeleteCompetitionsAsyncTask(competitionDao).execute(competition);
    }

    public void deleteAllCompetitions(){
        new DeleteAllCompetitionsNoteAsyncTask(competitionDao).execute();
    }

    public LiveData<List<Competition>> getCompetitions(){
        fetchCompetitions();
        competitions = competitionDao.getAllCompetitions();
        return competitions;
    }

    private void fetchCompetitions() {

        ConnectionEndpoint connectionEndpoint = ConnectionSetup.retrofit.create(ConnectionEndpoint.class);
        connectionEndpoint.getCompetitions()
                .enqueue(new Callback<CompetitionResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<CompetitionResponse> call, @NonNull Response<CompetitionResponse> response) {
                        if(response.body() != null) {
                            Log.d("Fetch", "onResponse: " + response.body().getCompetitions().get(0).getName());
                            parseCompetitions(response.body());
                        } else {
                            Log.d("Fetch Failed", "onResponse: ");
                            parseCompetitions(new CompetitionResponse());
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<CompetitionResponse> call, @NonNull Throwable t) {
                        Log.d("Fetch", "onResponse: " + t.getMessage());
                        parseCompetitions(new CompetitionResponse());
                    }
                });
    }

    private void parseCompetitions(CompetitionResponse competitionResponse){
        deleteAllCompetitions();
        List<Competition> competitions = competitionResponse.getCompetitions();
        for(Competition competition: competitions){
            insert(competition);
        }
    }

    private static class InsertCompetitionAsyncTask extends AsyncTask<Competition, Void, Void> {
        private CompetitionDao competitionDao;

        private InsertCompetitionAsyncTask(CompetitionDao competitionDao){
            this.competitionDao = competitionDao;
        }

        @Override
        protected Void doInBackground(Competition... competitions) {
            competitionDao.insert(competitions[0]);
            return null;
        }
    }

    private static class UpdateCompetitionAsyncTask extends AsyncTask<Competition, Void, Void> {
        private CompetitionDao competitionDao;

        private UpdateCompetitionAsyncTask(CompetitionDao competitionDao){
            this.competitionDao = competitionDao;
        }

        @Override
        protected Void doInBackground(Competition... competitions) {
            competitionDao.update(competitions[0]);
            return null;
        }
    }

    private static class DeleteCompetitionsAsyncTask extends AsyncTask<Competition, Void, Void> {
        private CompetitionDao competitionDao;

        private DeleteCompetitionsAsyncTask(CompetitionDao competitionDao){
            this.competitionDao = competitionDao;
        }

        @Override
        protected Void doInBackground(Competition... competitions) {
            competitionDao.delete(competitions[0]);
            return null;
        }
    }

    private static class DeleteAllCompetitionsNoteAsyncTask extends AsyncTask<Void, Void, Void> {
        private CompetitionDao competitionDao;

        private DeleteAllCompetitionsNoteAsyncTask(CompetitionDao competitionDao){
            this.competitionDao = competitionDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            competitionDao.deleteAllCompetitions();
            return null;
        }
    }
}
