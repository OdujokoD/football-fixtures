package com.odujokod.footballfixtures.adapter;

import android.content.Context;
import android.graphics.drawable.PictureDrawable;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.odujokod.footballfixtures.R;
import com.odujokod.footballfixtures.model.Team;
import com.odujokod.footballfixtures.util.SvgSoftwareLayerSetter;

import java.util.ArrayList;
import java.util.List;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class TeamAdapter extends RecyclerView.Adapter<TeamAdapter.TeamHolder>{

    private Context context;
    private List<Team> teams = new ArrayList<>();
    private static TeamAdapter.ClickListener clickListener;

    @NonNull
    @Override
    public TeamHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        context = viewGroup.getContext();
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.team_list, viewGroup, false);

        return new TeamHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull TeamHolder teamHolder, int position) {
        Team team = teams.get(position);
        teamHolder.bindData(team);
    }

    @Override
    public int getItemCount() {
        return teams.size();
    }

    public void setTeams(List<Team> teams){
        this.teams = teams;
        notifyDataSetChanged();
    }

    public void setClickListener(ClickListener clickListener) {
        TeamAdapter.clickListener = clickListener;
    }

    public Team getTeamAt(int position){
        return teams.get(position);
    }

    public interface ClickListener {
        void itemClicked(View view, int position);
    }

    class TeamHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView teamName;
        private ImageView teamIcon;

        TeamHolder(@NonNull View itemView) {
            super(itemView);

            teamIcon = itemView.findViewById(R.id.iv_team_logo);
            teamName = itemView.findViewById(R.id.tv_team_short_name);

            itemView.setOnClickListener(this);
        }

        void bindData(Team team){
            teamName.setText(team.getShortName());

            RequestBuilder<PictureDrawable> requestBuilder = Glide.with(context)
                    .as(PictureDrawable.class)
                    .error(R.drawable.android_head)
                    .transition(withCrossFade())
                    .listener(new SvgSoftwareLayerSetter());

            Uri uri = Uri.parse(team.getCrestUrl());
            requestBuilder.load(uri).into(teamIcon);


        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) {
                Log.d("Adapter", "onClick: ");
                clickListener.itemClicked(view, this.getLayoutPosition());
            }
        }
    }
}
