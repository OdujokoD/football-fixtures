package com.odujokod.footballfixtures.adapter;

import android.content.Context;
import android.graphics.drawable.PictureDrawable;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.odujokod.footballfixtures.R;
import com.odujokod.footballfixtures.model.Standing;
import com.odujokod.footballfixtures.model.Team;
import com.odujokod.footballfixtures.util.SvgSoftwareLayerSetter;

import java.util.ArrayList;
import java.util.List;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;


public class TableAdapter extends RecyclerView.Adapter<TableAdapter.TableHolder>{

    private Context context;
    private List<Standing.Table> tables = new ArrayList<>();

    @NonNull
    @Override
    public TableHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        context = viewGroup.getContext();
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.table_items, viewGroup, false);

        return new TableHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull TableHolder tableHolder, int position) {
        Standing.Table table = tables.get(position);
        tableHolder.bindData(table);
    }

    @Override
    public int getItemCount() {
        return tables.size();
    }

    public void setTables(List<Standing.Table> tables){
        this.tables = tables;
        notifyDataSetChanged();
    }

    class TableHolder extends RecyclerView.ViewHolder {

        private TextView teamPosition;
        private ImageView teamIcon;
        private TextView teamName;
        private TextView teamGamesPlayed;
        private TextView teamGoalsFor;
        private TextView teamPoint;

        TableHolder(@NonNull View itemView) {
            super(itemView);

            teamPosition = itemView.findViewById(R.id.tv_shirt_number);
            teamIcon = itemView.findViewById(R.id.iv_club_logo);
            teamName = itemView.findViewById(R.id.tv_team_name);
            teamGamesPlayed = itemView.findViewById(R.id.tv_games_played);
            teamGoalsFor = itemView.findViewById(R.id.tv_goals_for);
            teamPoint = itemView.findViewById(R.id.tv_player_role);
        }

        void bindData(Standing.Table table){
            String position = String.valueOf(table.getPosition());
            String gamesPlayed = String.valueOf(table.getPlayedGames());
            String goalsFor = String.valueOf(table.getGoalsFor());
            String point = String.valueOf(table.getPoints());

            Team team = table.getTeam();

            teamPosition.setText(position);
            teamName.setText(team.getName());
            teamGamesPlayed.setText(gamesPlayed);
            teamGoalsFor.setText(goalsFor);
            teamPoint.setText(point);

            RequestBuilder<PictureDrawable> requestBuilder = Glide.with(context)
                    .as(PictureDrawable.class)
                    .error(R.drawable.android_head)
                    .transition(withCrossFade())
                    .listener(new SvgSoftwareLayerSetter());

            Uri uri = Uri.parse(team.getCrestUrl());
            requestBuilder.load(uri).into(teamIcon);


        }
    }


}
