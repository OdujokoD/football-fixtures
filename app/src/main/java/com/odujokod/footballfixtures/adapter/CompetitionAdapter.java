package com.odujokod.footballfixtures.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.odujokod.footballfixtures.R;
import com.odujokod.footballfixtures.model.Competition;

import java.util.ArrayList;
import java.util.List;

public class CompetitionAdapter extends RecyclerView.Adapter<CompetitionAdapter.CompetitionHolder> {

    private static ClickListener clickListener;
    private List<Competition> competitions = new ArrayList<>();

    @NonNull
    @Override
    public CompetitionHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.competition_list, viewGroup, false);

        return new CompetitionAdapter.CompetitionHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CompetitionHolder competitionHolder, int position) {
        Competition competition = competitions.get(position);
        competitionHolder.bindData(competition);
    }

    @Override
    public int getItemCount() {
        return competitions.size();
    }

    public void setCompetitions(List<Competition> competitions){
        this.competitions = competitions;
        notifyDataSetChanged();
    }

    public void setClickListener(ClickListener clickListener) {
        CompetitionAdapter.clickListener = clickListener;
    }

    public Competition getCompetitionAt(int position){
        return competitions.get(position);
    }

    public interface ClickListener {
        void itemClicked(View view, int position);
    }

    class CompetitionHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView competitionName;

        CompetitionHolder(@NonNull View itemView) {
            super(itemView);

            competitionName = itemView.findViewById(R.id.tv_shirt_number);
            itemView.setOnClickListener(this);
        }

        void bindData(Competition competition){
            competitionName.setText(competition.getName());
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) {
                clickListener.itemClicked(view, this.getLayoutPosition());
            }
        }
    }
}
