package com.odujokod.footballfixtures.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.odujokod.footballfixtures.R;
import com.odujokod.footballfixtures.model.Match;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MatchAdapter extends RecyclerView.Adapter<MatchAdapter.MatchHolder> {

    private List<Match> matches = new ArrayList<>();

    @NonNull
    @Override
    public MatchHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.match_list, viewGroup, false);

        return new MatchHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MatchHolder matchHolder, int position) {
        Match match = matches.get(position);
        matchHolder.bindData(match);
    }

    @Override
    public int getItemCount() {
        return matches.size();
    }

    public void setMatches(List<Match> matches){
        this.matches = matches;
        notifyDataSetChanged();
    }

    class MatchHolder extends RecyclerView.ViewHolder {

        private TextView matchStatus;
        private TextView matchTime;
        private TextView matchDay;
        private TextView matchHomeTeam;
        private TextView matchAwayTeam;
        private TextView matchHalfTimeHome;
        private TextView matchFullTimeHome;
        private TextView matchHalfTimeAway;
        private TextView matchFullTimeAway;

        MatchHolder(@NonNull View itemView) {
            super(itemView);

            matchStatus = itemView.findViewById(R.id.tv_match_status);
            matchTime = itemView.findViewById(R.id.tv_match_time);
            matchDay = itemView.findViewById(R.id.tv_match_day);
            matchHomeTeam = itemView.findViewById(R.id.tv_home_team);
            matchAwayTeam = itemView.findViewById(R.id.tv_away_team);
            matchHalfTimeHome = itemView.findViewById(R.id.tv_home_score_half_time);
            matchFullTimeHome = itemView.findViewById(R.id.tv_home_score_full_time);
            matchHalfTimeAway = itemView.findViewById(R.id.tv_away_score_half_time);
            matchFullTimeAway = itemView.findViewById(R.id.tv_away_score_full_time);
        }

        void bindData(Match match){
            Date date = match.getUtcDate();
            String mMatchTime = String.format("%tR", date);
            String mMatchDay = "MD: " + String.valueOf(match.getMatchday());

            matchStatus.setText(match.getStatus());
            matchTime.setText(mMatchTime);
            matchDay.setText(mMatchDay);
            matchHomeTeam.setText(match.getHomeTeam().getName());
            matchAwayTeam.setText(match.getAwayTeam().getName());
            matchHalfTimeHome.setText(String.valueOf(match.getScore().getHalfTime().getHomeTeam()));
            matchFullTimeHome.setText(String.valueOf(match.getScore().getFullTime().getHomeTeam()));
            matchHalfTimeAway.setText(String.valueOf(match.getScore().getHalfTime().getAwayTeam()));
            matchFullTimeAway.setText(String.valueOf(match.getScore().getFullTime().getAwayTeam()));

        }
    }
}
