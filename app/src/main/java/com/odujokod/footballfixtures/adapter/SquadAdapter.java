package com.odujokod.footballfixtures.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.odujokod.footballfixtures.R;
import com.odujokod.footballfixtures.model.Squad;

import java.util.ArrayList;
import java.util.List;

public class SquadAdapter extends RecyclerView.Adapter<SquadAdapter.SquadHolder> {

    private List<Squad> teamSquad = new ArrayList<>();

    @NonNull
    @Override
    public SquadHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.squad_list, viewGroup, false);

        return new SquadHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SquadHolder squadHolder, int position) {
        squadHolder.bindData(position);
    }

    @Override
    public int getItemCount() {
        return teamSquad.size();
    }

    public void setSquad(List<Squad> squad){
        teamSquad = squad;
        notifyDataSetChanged();
    }

    class SquadHolder extends RecyclerView.ViewHolder {

        private TextView shirtNumber;
        private TextView playerName;
        private TextView playerRole;

        SquadHolder(@NonNull View itemView) {
            super(itemView);

            shirtNumber = itemView.findViewById(R.id.tv_squad_shirt_number);
            playerName = itemView.findViewById(R.id.tv_squad_player_name);
            playerRole = itemView.findViewById(R.id.tv_player_role);
        }

        void bindData(int position){
            Squad squad = teamSquad.get(position);
            Log.d("SQuadHolder", "bindData: " + squad.getName());
            shirtNumber.setText(String.valueOf(position));
            playerName.setText(squad.getName());
            playerRole.setText(squad.getPosition());

        }
    }
}
