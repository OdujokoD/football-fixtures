package com.odujokod.footballfixtures.view;

import android.graphics.drawable.PictureDrawable;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.odujokod.footballfixtures.network.ConnectionEndpoint;
import com.odujokod.footballfixtures.network.ConnectionSetup;
import com.odujokod.footballfixtures.R;
import com.odujokod.footballfixtures.adapter.SquadAdapter;
import com.odujokod.footballfixtures.model.Squad;
import com.odujokod.footballfixtures.model.Team;
import com.odujokod.footballfixtures.util.SvgSoftwareLayerSetter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;
import static com.odujokod.footballfixtures.view.TeamFragment.EXTRA_CREST_URL;
import static com.odujokod.footballfixtures.view.TeamFragment.EXTRA_TEAM_ID;
import static com.odujokod.footballfixtures.view.TeamFragment.EXTRA_TEAM_NAME;

public class BottomSheetFragment extends BottomSheetDialogFragment {

    private SquadAdapter squadAdapter;
    private BottomSheetBehavior sheetBehavior;

    public BottomSheetFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bottom_sheet, container, false);

        TextView teamName = view.findViewById(R.id.tv_btm_team_name);
        ImageView crestUrl = view.findViewById(R.id.iv_btm_team_icon);

        RecyclerView recyclerView = view.findViewById(R.id.rv_squad);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);

        squadAdapter = new SquadAdapter();
        recyclerView.setAdapter(squadAdapter);

        if (getArguments() != null) {
            fetchSquad(getArguments().getInt(EXTRA_TEAM_ID));
            teamName.setText(getArguments().getString(EXTRA_TEAM_NAME));

            RequestBuilder<PictureDrawable> requestBuilder = Glide.with(view.getContext())
                    .as(PictureDrawable.class)
                    .error(R.drawable.android_head)
                    .transition(withCrossFade())
                    .listener(new SvgSoftwareLayerSetter());

            Uri uri = Uri.parse(getArguments().getString(EXTRA_CREST_URL));
            requestBuilder.load(uri).into(crestUrl);
        }

//        CoordinatorLayout layoutBottomSheet = view.findViewById(R.id.cl_bottom_sheet);
//        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);
//
//        initBottomSheetBehavior();

        return view;
    }

    private void initBottomSheetBehavior() {
        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        break;
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
    }

    private void fetchSquad(int id) {

        ConnectionEndpoint connectionEndpoint = ConnectionSetup.retrofit.create(ConnectionEndpoint.class);
        connectionEndpoint.getSquad(id)
                .enqueue(new Callback<Team>() {
                    @Override
                    public void onResponse(@NonNull Call<Team> call, @NonNull Response<Team> response) {
                        if (response.body() != null) {
                            parseSquad(response.body());
                        } else {
                            Toast.makeText(getActivity(), "Data retrieval failed!", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<Team> call, @NonNull Throwable t) {
                        Toast.makeText(getActivity(), "Data retrieval failed!", Toast.LENGTH_SHORT).show();
                        Log.d("onFailure", t.getMessage());
                    }
                });
    }

    private void parseSquad(Team team) {
        List<Squad> squad = team.getSquad();

        squadAdapter.setSquad(squad);
    }
}
