package com.odujokod.footballfixtures.view;


import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.odujokod.footballfixtures.network.ConnectionEndpoint;
import com.odujokod.footballfixtures.network.ConnectionSetup;
import com.odujokod.footballfixtures.R;
import com.odujokod.footballfixtures.adapter.TableAdapter;
import com.odujokod.footballfixtures.model.Standing;
import com.odujokod.footballfixtures.model.StandingResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TableFragment extends Fragment {

    private TableAdapter tableAdapter;

    public TableFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_table, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.rv_table);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);

        tableAdapter = new TableAdapter();
        recyclerView.setAdapter(tableAdapter);

        if (getArguments() != null) {
            int competitionId = getArguments().getInt(CompetitionFragment.EXTRA_COMPETITION_ID);

            fetchStandings(competitionId);
        }
        return view;
    }

    private void fetchStandings(int id) {

        ConnectionEndpoint connectionEndpoint = ConnectionSetup.retrofit.create(ConnectionEndpoint.class);
        connectionEndpoint.getStandings(2021)
                .enqueue(new Callback<StandingResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<StandingResponse> call, @NonNull Response<StandingResponse> response) {
                        if(response.body() != null) {
                            parseStandings(response.body());
                            Toast.makeText(getActivity(), "onSuccess", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), "onResponse" + response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<StandingResponse> call, @NonNull Throwable t) {
                        Toast.makeText(getActivity(), "onFailure", Toast.LENGTH_SHORT).show();;
                    }
                });
    }

    private void parseStandings(StandingResponse standingResponse){
        List<Standing.Table> tables = standingResponse.getstandings().get(0).getTable();

        tableAdapter.setTables(tables);

    }

}
