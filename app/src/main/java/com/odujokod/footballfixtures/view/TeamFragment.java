package com.odujokod.footballfixtures.view;


import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.odujokod.footballfixtures.network.ConnectionEndpoint;
import com.odujokod.footballfixtures.network.ConnectionSetup;
import com.odujokod.footballfixtures.R;
import com.odujokod.footballfixtures.adapter.TeamAdapter;
import com.odujokod.footballfixtures.model.Team;
import com.odujokod.footballfixtures.model.TeamResponse;

import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TeamFragment extends Fragment implements TeamAdapter.ClickListener {

    private TeamAdapter teamAdapter;
    public static final String EXTRA_TEAM_ID = "com.odujokod.footballfixtures.view_EXTRA_TEAM_ID";
    public static final String EXTRA_TEAM_NAME = "com.odujokod.footballfixtures.view_EXTRA_TEAM_NAME";
    public static final String EXTRA_CREST_URL= "com.odujokod.footballfixtures.view_EXTRA_CREST_URL";

    public TeamFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_team, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.rv_teams);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        recyclerView.setHasFixedSize(true);

        teamAdapter = new TeamAdapter();
        recyclerView.setAdapter(teamAdapter);
        teamAdapter.setClickListener(this);

        if (getArguments() != null) {
            int competitionId = getArguments().getInt(CompetitionFragment.EXTRA_COMPETITION_ID);

            fetchTeams(competitionId);
        }
        return view;
    }

    @Override
    public void itemClicked(View view, int position) {
        showBottomSheetDialog(position);
    }

    public void showBottomSheetDialog(int position) {
        int teamId = teamAdapter.getTeamAt(position).getId();
        String teamName = teamAdapter.getTeamAt(position).getName();
        String crestUrl = teamAdapter.getTeamAt(position).getCrestUrl();

        Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_TEAM_ID, teamId);
        bundle.putString(EXTRA_TEAM_NAME, teamName);
        bundle.putString(EXTRA_CREST_URL, crestUrl);

        BottomSheetFragment bottomSheetFragment = new BottomSheetFragment();
        bottomSheetFragment.setArguments(bundle);

        bottomSheetFragment.show(Objects.requireNonNull(getFragmentManager()), bottomSheetFragment.getTag());
    }

    private void fetchTeams(int id) {

        ConnectionEndpoint connectionEndpoint = ConnectionSetup.retrofit.create(ConnectionEndpoint.class);
        connectionEndpoint.getTeams(2021)
                .enqueue(new Callback<TeamResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<TeamResponse> call, @NonNull Response<TeamResponse> response) {
                        if (response.body() != null) {
                            parseTeams(response.body());
                            Toast.makeText(getActivity(), "onSuccess", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), "onResponse" + response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<TeamResponse> call, @NonNull Throwable t) {
                        Toast.makeText(getActivity(), "onFailure", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void parseTeams(TeamResponse teamResponse) {
        List<Team> teams = teamResponse.getTeams();

        teamAdapter.setTeams(teams);

    }

}
