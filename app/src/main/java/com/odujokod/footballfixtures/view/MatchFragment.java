package com.odujokod.footballfixtures.view;


import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.odujokod.footballfixtures.network.ConnectionEndpoint;
import com.odujokod.footballfixtures.network.ConnectionSetup;
import com.odujokod.footballfixtures.R;
import com.odujokod.footballfixtures.adapter.MatchAdapter;
import com.odujokod.footballfixtures.model.Match;
import com.odujokod.footballfixtures.model.MatchResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MatchFragment extends Fragment {

    private MatchAdapter matchAdapter;

    public MatchFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView = inflater.inflate(R.layout.fragment_match, container, false);

        RecyclerView recyclerView = mView.findViewById(R.id.rv_matches);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);

        matchAdapter = new MatchAdapter();
        recyclerView.setAdapter(matchAdapter);

        fetchMatches();

        return mView;
    }


    private void fetchMatches() {

        ConnectionEndpoint connectionEndpoint = ConnectionSetup.retrofit.create(ConnectionEndpoint.class);
        connectionEndpoint.getMatches()
                .enqueue(new Callback<MatchResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<MatchResponse> call, @NonNull Response<MatchResponse> response) {
                        if(response.body() != null) {
                            parseMatches(response.body());
                        } else {
                            Toast.makeText(getActivity(), "Data retrieval failed!", Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<MatchResponse> call, @NonNull Throwable t) {
                        Toast.makeText(getActivity(), "Data retrieval failed!", Toast.LENGTH_SHORT).show();
                        Log.d("onFailure", t.getMessage());
                    }
                });
    }

    private void parseMatches(MatchResponse matchResponse){
        List<Match> matches = new ArrayList<>(matchResponse.getMatches());

        matchAdapter.setMatches(matches);

    }

}
