package com.odujokod.footballfixtures.view;


import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.odujokod.footballfixtures.R;
import com.odujokod.footballfixtures.adapter.CompetitionAdapter;
import com.odujokod.footballfixtures.model.Competition;
import com.odujokod.footballfixtures.viewmodel.CompetitionViewModel;

import java.util.List;
import java.util.Objects;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

public class CompetitionFragment extends Fragment implements CompetitionAdapter.ClickListener{

    private CompetitionAdapter competitionAdapter;

    static final String EXTRA_COMPETITION_ID = "com.odujokod.footballfixtures.view_COMPETITION_ID";
    static final String EXTRA_COMPETITION_NAME = "com.odujokod.footballfixtures.view_COMPETITION_NAME";

    public CompetitionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_competition, container, false);

        RecyclerView recyclerView = mView.findViewById(R.id.rv_competitions);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);

        competitionAdapter = new CompetitionAdapter();
        recyclerView.setAdapter(competitionAdapter);
        competitionAdapter.setClickListener(this);

        Log.d("FRA", "onCreateView: ");

        CompetitionViewModel competitionViewModel = ViewModelProviders.of(Objects.requireNonNull(getActivity()))
                .get(CompetitionViewModel.class);
        competitionViewModel.getAllCompetitions().observe(this, new Observer<List<Competition>>() {
            @Override
            public void onChanged(List<Competition> competitions) {
                Log.d("Fetch", "Fragment: " + competitions.size());
                competitionAdapter.setCompetitions(competitions);
            }
        });

        // fetchCompetitions();

        return mView;
    }

    @Override
    public void itemClicked(View view, int position) {
        Competition competition = competitionAdapter.getCompetitionAt(position);
        Intent intent = new Intent(getActivity(), CompetitionDetailsActivity.class);
        intent.putExtra(EXTRA_COMPETITION_ID, competition.getId());
        intent.putExtra(EXTRA_COMPETITION_NAME, competition.getName());
        startActivity(intent);
    }

    /*private void fetchCompetitions() {

        ConnectionEndpoint connectionEndpoint = ConnectionSetup.retrofit.create(ConnectionEndpoint.class);
        connectionEndpoint.getmCompetitions()
                .enqueue(new Callback<CompetitionResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<CompetitionResponse> call, @NonNull Response<CompetitionResponse> response) {
                        if(response.body() != null) {
                             parseCompetitions(response.body());
                        } else {
                            Toast.makeText(getActivity(), "Data retrieval failed!", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<CompetitionResponse> call, @NonNull Throwable t) {
                        Toast.makeText(getActivity(), "Data retrieval failed!", Toast.LENGTH_SHORT).show();
                        Log.d("onFailure", t.getMessage());
                    }
                });
    }

    private void parseCompetitions(CompetitionResponse competitonResponse){
        List<Competition> competitions = new ArrayList<>(competitonResponse.getmCompetitions());

        competitionAdapter.setCompetitions(competitions);


    }*/
}
