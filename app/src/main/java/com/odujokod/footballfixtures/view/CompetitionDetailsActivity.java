package com.odujokod.footballfixtures.view;

import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.odujokod.footballfixtures.R;
import com.odujokod.footballfixtures.adapter.SectionsPageAdapter;

public class CompetitionDetailsActivity extends AppCompatActivity {

    private int competitionId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_competition_details);

        final Intent intent = getIntent();
        competitionId= intent.getIntExtra(CompetitionFragment.EXTRA_COMPETITION_ID, 1);

        Toolbar toolbar = findViewById(R.id.tb_competition_name);
        setSupportActionBar(toolbar);
        toolbar.setTitle(intent.getStringExtra(CompetitionFragment.EXTRA_COMPETITION_NAME));

        ViewPager viewPager = findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        TabLayout tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager){
        SectionsPageAdapter adapter = new SectionsPageAdapter(getSupportFragmentManager());
        Bundle bundle = new Bundle();
        bundle.putInt(CompetitionFragment.EXTRA_COMPETITION_ID, competitionId);

        TableFragment tableFragment = new TableFragment();
        tableFragment.setArguments(bundle);

        FixturesFragment fixturesFragment = new FixturesFragment();
        fixturesFragment.setArguments(bundle);

        TeamFragment teamFragment = new TeamFragment();
        teamFragment.setArguments(bundle);

        adapter.addFragment(tableFragment, "Table");
        adapter.addFragment(fixturesFragment, "Fixtures");
        adapter.addFragment(teamFragment, "Teams");

        viewPager.setAdapter(adapter);
    }
}
