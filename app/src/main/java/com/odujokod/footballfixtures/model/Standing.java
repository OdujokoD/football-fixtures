package com.odujokod.footballfixtures.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Standing {
    @SerializedName("table")
    private List<Table> table;

    public List<Table> getTable() {
        return table;
    }

    public class Table {
        @SerializedName("position")
        int position;

        @SerializedName("team")
        Team team;

        @SerializedName("playedGames")
        int playedGames;

        @SerializedName("points")
        int points;

        @SerializedName("goalsFor")
        int goalsFor;

        public int getPosition() {
            return position;
        }

        public Team getTeam() {
            return team;
        }

        public int getPlayedGames() {
            return playedGames;
        }

        public int getPoints() {
            return points;
        }

        public int getGoalsFor() {
            return goalsFor;
        }
    }
}
