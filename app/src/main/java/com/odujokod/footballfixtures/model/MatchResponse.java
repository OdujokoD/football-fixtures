package com.odujokod.footballfixtures.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MatchResponse {
    @SerializedName("matches")
    private List<Match> matches;

    public List<Match> getMatches() {
        return matches;
    }
}
