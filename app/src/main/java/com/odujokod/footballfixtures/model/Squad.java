package com.odujokod.footballfixtures.model;

import com.google.gson.annotations.SerializedName;

public class Squad {
    @SerializedName("name")
    private String name;

    @SerializedName("position")
    private String position;

    public String getName() {
        return name;
    }

    public String getPosition() {
        return position;
    }
}
