package com.odujokod.footballfixtures.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Team {
    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("shortName")
    private String shortName;

    @SerializedName("crestUrl")
    private String crestUrl;

    @SerializedName("squad")
    private List<Squad> squad;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getShortName() {
        return shortName;
    }

    public String getCrestUrl() {
        return crestUrl;
    }

    public List<Squad> getSquad() {
        return squad;
    }
}
