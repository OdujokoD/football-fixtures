package com.odujokod.footballfixtures.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StandingResponse {

    @SerializedName("standings")
    private List<Standing> standings;

    public List<Standing> getstandings() {
        return standings;
    }
}
