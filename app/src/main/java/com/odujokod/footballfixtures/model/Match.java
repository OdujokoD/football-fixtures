package com.odujokod.footballfixtures.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

public class Match {
    @SerializedName("id")
    private int id;

    @SerializedName("utcDate")
    private Date utcDate;

    @SerializedName("status")
    private String status;

    @SerializedName("matchday")
    private int matchday;

    @SerializedName("homeTeam")
    private Team homeTeam;

    @SerializedName("awayTeam")
    private Team awayTeam;

    @SerializedName("score")
    private Score score;

    public int getId() {
        return id;
    }

    public Date getUtcDate() {
        return utcDate;
    }

    public String getStatus() {
        return status;
    }

    public int getMatchday() {
        return matchday;
    }

    public Team getHomeTeam() {
        return homeTeam;
    }

    public Team getAwayTeam() {
        return awayTeam;
    }

    public Score getScore() {
        return score;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUtcDate(Date utcDate) {
        this.utcDate = utcDate;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setMatchday(int matchday) {
        this.matchday = matchday;
    }

    public void setHomeTeam(Team homeTeam) {
        this.homeTeam = homeTeam;
    }

    public void setAwayTeam(Team awayTeam) {
        this.awayTeam = awayTeam;
    }

    public void setScore(Score score) {
        this.score = score;
    }

    public class Score {
        @SerializedName("fullTime")
        MatchTime fullTime;

        @SerializedName("halfTime")
        MatchTime halfTime;

        public MatchTime getFullTime() {
            return fullTime;
        }

        public MatchTime getHalfTime() {
            return halfTime;
        }
    }

    public class MatchTime {
        @SerializedName("homeTeam")
        String homeTeam;

        @SerializedName("awayTeam")
        String awayTeam;

        public String getHomeTeam() {
            return homeTeam;
        }

        public String getAwayTeam() {
            return awayTeam;
        }
    }


}
