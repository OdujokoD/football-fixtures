package com.odujokod.footballfixtures.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CompetitionResponse {
    @SerializedName("competitions")
    private List<Competition> competitions;

    public List<Competition> getCompetitions() {
        return competitions;
    }
}
