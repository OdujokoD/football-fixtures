package com.odujokod.footballfixtures.db;

import com.odujokod.footballfixtures.model.Competition;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface CompetitionDao {

    @Insert
    void insert(Competition competition);

    @Update
    void update(Competition competition);

    @Delete
    void delete(Competition competition);

    @Query("DELETE FROM competition")
    void deleteAllCompetitions();

    @Query("SELECT * FROM competition")
    LiveData<List<Competition>> getAllCompetitions();
}
