package com.odujokod.footballfixtures.db;

import android.content.Context;

import com.odujokod.footballfixtures.model.Competition;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {Competition.class}, version = 1)
public abstract class FootballFixtureDB extends RoomDatabase {

    private static FootballFixtureDB instance;

    public abstract CompetitionDao competitionDao();

    public static synchronized FootballFixtureDB getInstance(Context context){
        if (instance == null){
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    FootballFixtureDB.class, "football_fixtures")
                    .fallbackToDestructiveMigration()
                    .build();
        }

        return instance;
    }
}
