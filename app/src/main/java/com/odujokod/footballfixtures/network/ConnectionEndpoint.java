package com.odujokod.footballfixtures.network;

import com.odujokod.footballfixtures.BuildConfig;
import com.odujokod.footballfixtures.model.CompetitionResponse;
import com.odujokod.footballfixtures.model.MatchResponse;
import com.odujokod.footballfixtures.model.StandingResponse;
import com.odujokod.footballfixtures.model.Team;
import com.odujokod.footballfixtures.model.TeamResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;

public interface ConnectionEndpoint {
    @Headers({"X-Auth-Token: " + BuildConfig.API_KEY})
    @GET("matches?dateFrom=2019-02-13&dateTo=2019-02-14")
    Call<MatchResponse> getMatches();

    @Headers({"X-Auth-Token: " + BuildConfig.API_KEY})
    @GET("competitions")
    Call<CompetitionResponse> getCompetitions();

    @Headers({"X-Auth-Token: " + BuildConfig.API_KEY})
    @GET("competitions/{id}/standings?standingType=HOME")
    Call<StandingResponse> getStandings(@Path("id") int id);

    @Headers({"X-Auth-Token: " + BuildConfig.API_KEY})
    @GET("competitions/{id}/teams")
    Call<TeamResponse> getTeams(@Path("id") int id);

    @Headers({"X-Auth-Token: " + BuildConfig.API_KEY})
    @GET("teams/{id}")
    Call<Team> getSquad(@Path("id") int id);
}
